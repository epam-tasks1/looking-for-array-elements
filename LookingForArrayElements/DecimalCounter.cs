﻿using System;

#pragma warning disable S2368

namespace LookingForArrayElements
{
    public static class DecimalCounter
    {
        public static int GetDecimalsCount(decimal[] arrayToSearch, decimal[][] ranges)
        {
            if (arrayToSearch is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch));
            }

            if (ranges is null)
            {
                throw new ArgumentNullException(nameof(ranges));
            }

            int countOfEqual = 0;
            int indexRange = 0;
            int indexArray = 0;
            int lengthOfRanges = 0;

            for (int i = 0; i < ranges.Length; i++)
            {
                if (ranges[i] is null)
                {
                    throw new ArgumentNullException(nameof(ranges));
                }

                if (ranges[i].Length > 2)
                {
                    throw new ArgumentException("Method throws ArgumentException in case the length of one of the ranges is less or greater than 2.");
                }

                if (ranges[i].Length != 0)
                {
                    lengthOfRanges++;
                }
            }

            if (arrayToSearch.Length == 0)
            {
                return 0;
            }

            if (ranges.Length == 0)
            {
                return 0;
            }

            do
            {
                do
                {
                    if (arrayToSearch[indexArray] >= ranges[indexRange][0] && arrayToSearch[indexArray] <= ranges[indexRange][1])
                    {
                        countOfEqual++;
                        break;
                    }

                    indexRange++;
                }
                while (indexRange != lengthOfRanges);

                indexRange = 0;

                indexArray++;
            }
            while (indexArray != arrayToSearch.Length);

            return countOfEqual;
        }

        public static int GetDecimalsCount(decimal[] arrayToSearch, decimal[][] ranges, int startIndex, int count)
        {
            if (arrayToSearch is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch));
            }

            if (ranges is null)
            {
                throw new ArgumentNullException(nameof(ranges));
            }

            if (ranges.Length == 0)
            {
                return 0;
            }

            if (count == 0)
            {
                return 0;
            }

            if (count < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(count));
            }

            for (int i = 0; i < ranges.Length; i++)
            {
                if (ranges[i] is null)
                {
                    throw new ArgumentNullException(nameof(ranges));
                }

                if (ranges[i].Length > 2)
                {
                    throw new ArgumentException("Method throws ArgumentException in case the length of one of the ranges is less or greater than 2.");
                }
            }

            if (startIndex + count > arrayToSearch.Length || startIndex < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex));
            }

            int countInRange = 0;

            for (int i = startIndex; i < startIndex + count; i++)
            {
                for (int j = 0; j < ranges.Length; j++)
                {
                    if (arrayToSearch[i] >= ranges[j][0] && arrayToSearch[i] <= ranges[j][1])
                    {
                        countInRange++;
                        break;
                    }
                }
            }

            return countInRange;
        }
    }
}
