﻿using System;

namespace LookingForArrayElements
{
    public static class IntegersCounter
    {
        public static int GetIntegersCount(int[] arrayToSearch, int[] elementsToSearchFor)
        {
            if (arrayToSearch is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch));
            }

            if (elementsToSearchFor is null)
            {
                throw new ArgumentNullException(nameof(elementsToSearchFor));
            }

            int count = 0;
            for (int i = 0; i < elementsToSearchFor.Length; i++)
            {
                for (int j = 0; j < arrayToSearch.Length; j++)
                {
                    if (elementsToSearchFor[i] == arrayToSearch[j])
                    {
                        count++;
                    }
                }
            }

            return count;
        }

        public static int GetIntegersCount(int[] arrayToSearch, int[] elementsToSearchFor, int startIndex, int count)
        {
            if (arrayToSearch is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch));
            }

            if (elementsToSearchFor is null)
            {
                throw new ArgumentNullException(nameof(elementsToSearchFor));
            }

            if (startIndex < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), "startIndex is less than zero.");
            }

            if (startIndex > arrayToSearch.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(arrayToSearch), "arrayToSearck is greater than arrayToSearch.Length.");
            }

            if (startIndex + count > arrayToSearch.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(count), "startIndex + count > arrayToSearch.Length.");
            }

            if (count < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(count), "count is less than zero.");
            }

            int countOfEqual = 0;
            int indexI = startIndex;
            int indexJ = 0;
            while (indexI != startIndex + count)
            {
                while (indexJ != elementsToSearchFor.Length)
                {
                    if (arrayToSearch[indexI] == elementsToSearchFor[indexJ])
                    {
                        countOfEqual++;
                    }

                    indexJ++;
                }

                indexJ = 0;
                indexI++;
            }

            return countOfEqual;
        }
    }
}
